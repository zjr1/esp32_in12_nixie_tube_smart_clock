#ifndef __WIFI_H__
#define __WIFI_H__

// Forward declaration so <filesystem.h> isn't needed
typedef struct spiffs_configuration_struct spiffs_configuration_st;

bool setup_wifi(spiffs_configuration_st &config);

bool start_config_portal();

#endif
