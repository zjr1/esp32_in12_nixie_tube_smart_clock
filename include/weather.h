#ifndef __WEATHER_H__
#define __WEATHER_H__

#include <WString.h>

bool get_local_temperature(const String openweather_api_key,
                           double *temperature);

#endif
