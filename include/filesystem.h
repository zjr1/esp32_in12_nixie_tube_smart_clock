#ifndef __FILESYSTEM_H__
#define __FILESYSTEM_H__

#include <WString.h>

typedef struct spiffs_configuration_struct {
  String openweather_api_key;
} spiffs_configuration_st;

bool save_config_file(const spiffs_configuration_st& config);

bool load_config_file(spiffs_configuration_st* config);

#endif
