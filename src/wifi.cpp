#include "wifi.h"

#include <ArduinoJson.h>
#include <EEPROM.h>
#include <WiFi.h>
#include <WiFiManager.h>

#include "arduino_debug.h"
#include "config.h"
#include "filesystem.h"
#include "tasks.h"

#define ACCESS_POINT_NAME "ИН-12A Nixie Tube Clock"

#define CONFIG_PORTAL_TIMEOUT 300  // Seconds
#define CONNECT_TIMEOUT 10         // Seconds

#define OPENWEATHER_API_KEY_LEN 32

#define CREATE_WIFIMANAGER_PARAMETERS(wm, config)                            \
  uint8_t l_hour_format = EEPROM.read(EEPROM_12_HOUR_FORMAT_ADDRESS);        \
  char l_hour_format_str[4];                                                 \
  snprintf(l_hour_format_str, 2, "%d", l_hour_format);                       \
                                                                             \
  WiFiManagerParameter twelve_hour_format_param(                             \
      "id_twelve_hour_format_param",                                         \
      "Hour Format: [0 = 24 hour] [1 = 12 hour]", l_hour_format_str, 1);     \
                                                                             \
  wm.addParameter(&twelve_hour_format_param);                                \
                                                                             \
  uint8_t l_date_display_frequency =                                         \
      EEPROM.read(EEPROM_DATE_DISPLAY_FREQUENCY_ADDRESS);                    \
  char l_date_display_frequency_str[4];                                      \
  snprintf(l_date_display_frequency_str, 3, "%d", l_date_display_frequency); \
                                                                             \
  WiFiManagerParameter date_display_frequency_param(                         \
      "id_date_display_frequency",                                           \
      "Date Display Interval (minutes): [0 = disabled] [99 = max]",          \
      l_date_display_frequency_str, 2);                                      \
                                                                             \
  wm.addParameter(&date_display_frequency_param);                            \
                                                                             \
  uint8_t l_slot_machine_cycle_frequency =                                   \
      EEPROM.read(EEPROM_SLOT_MACHINE_CYCLE_FREQUENCY_ADDRESS);              \
  char l_slot_machine_cycle_frequency_str[4];                                \
  snprintf(l_slot_machine_cycle_frequency_str, 3, "%d",                      \
           l_slot_machine_cycle_frequency);                                  \
                                                                             \
  WiFiManagerParameter slot_machine_cycle_frequency_param(                   \
      "id_slot_machine_cycle_frequency",                                     \
      "Slot Machine Cycle Interval (minutes): [1 = min] [60 = max]",         \
      l_slot_machine_cycle_frequency_str, 2);                                \
                                                                             \
  wm.addParameter(&slot_machine_cycle_frequency_param);                      \
                                                                             \
  uint8_t l_local_temperature_display_frequency =                            \
      EEPROM.read(EEPROM_LOCAL_TEMPERATURE_DISPLAY_FREQUENCY_ADDRESS);       \
  char l_local_temperature_display_frequency_str[4];                         \
  snprintf(l_local_temperature_display_frequency_str, 3, "%d",               \
           l_local_temperature_display_frequency);                           \
                                                                             \
  WiFiManagerParameter local_temperature_display_frequency_param(            \
      "id_local_temperature_display_frequency",                              \
      "Local Temperature Display Interval (minutes): [5 = min] [99 = max]",  \
      l_local_temperature_display_frequency_str, 2);                         \
                                                                             \
  wm.addParameter(&local_temperature_display_frequency_param);               \
                                                                             \
  WiFiManagerParameter open_weather_api_key_param(                           \
      "id_open_weather_api_key", "OpenWeather API Key",                      \
      config.openweather_api_key.c_str(), OPENWEATHER_API_KEY_LEN);          \
                                                                             \
  wm.addParameter(&open_weather_api_key_param);

static void configure_wifi_manager(WiFiManager &wm);
static void save_config_callback();
static void validate_and_save_params(
    spiffs_configuration_st &config,
    const WiFiManagerParameter &twelve_hour_format_param,
    const WiFiManagerParameter &date_display_frequency_param,
    const WiFiManagerParameter &slot_machine_cycle_frequency_param,
    const WiFiManagerParameter &local_temperature_display_frequency_param,
    const WiFiManagerParameter &open_weather_api_key_param);
static void set_eeprom_config_value_from_param(
    const WiFiManagerParameter &param, uint8_t option_number,
    uint8_t lower_bound, uint8_t upper_bound, TaskHandle_t *task_handle,
    const char *error_msg_format_str);

bool m_save_config = false;

bool setup_wifi(spiffs_configuration_st &config) {
  WiFi.mode(WIFI_STA);  // explicitly set mode, esp defaults to STA+AP
  // it is a good practice to make sure your code sets wifi mode how you want it

  // WiFiManager, Local intialization. Once its business is done, there is no
  // need to keep it around
  WiFiManager wm;

  // reset settings - wipe stored credentials for testing
  //                  these are stored by the esp library
  // wm.resetSettings();

  configure_wifi_manager(wm);

  CREATE_WIFIMANAGER_PARAMETERS(wm, config)

  // Automatically connect using saved credentials,
  // if connection fails, it starts an access point with the specified name (
  // ACCESS_POINT_NAME), if empty will auto generate SSID, if password is blank
  // it will be anonymous AP (wm.autoConnect()) then goes into a blocking loop
  // awaiting configuration and will return success result

  bool res;
  // res = wm.autoConnect(); // auto generated AP name from chipid
  res = wm.autoConnect(ACCESS_POINT_NAME);  // anonymous ap
  // res = wm.autoConnect(ACCESS_POINT_NAME, "password");  // password prot ap

  validate_and_save_params(
      config, twelve_hour_format_param, date_display_frequency_param,
      slot_machine_cycle_frequency_param,
      local_temperature_display_frequency_param, open_weather_api_key_param);

  if (!res) {
    debug_serial_println("Failed to connect. Restarting...");
    delay(2000);
    ESP.restart();
    return false;  // Should never be reached
  }

  // if you get here you have connected to the WiFi
  debug_serial_println("Connected to wifi!");

  return true;
}

bool start_config_portal() {
  WiFiManager wm;

  // reset settings - for testing
  // wm.resetSettings();

  configure_wifi_manager(wm);

  // Additional config for this specific call
  wm.setConfigPortalTimeout(CONFIG_PORTAL_TIMEOUT);

  spiffs_configuration_st config;
  load_config_file(&config);

  CREATE_WIFIMANAGER_PARAMETERS(wm, config)

  bool res = wm.startConfigPortal(ACCESS_POINT_NAME);

  validate_and_save_params(
      config, twelve_hour_format_param, date_display_frequency_param,
      slot_machine_cycle_frequency_param,
      local_temperature_display_frequency_param, open_weather_api_key_param);

  if (!res) {
    debug_serial_println("wm.startConfigPortal exited or timed out.");

    if (!WiFi.isConnected()) {
      debug_serial_println("Failed to connect. Restarting...");
      delay(2000);
      ESP.restart();
      return false;  // Should never be reached
    }
  }

  // If you get here you have connected to the WiFi
  debug_serial_println("Connected to wifi!");

  return true;
}

static void configure_wifi_manager(WiFiManager &wm) {
  // Set dark theme
  wm.setClass("invert");

  wm.setConnectTimeout(CONNECT_TIMEOUT);

  wm.setParamsPage(true);

  wm.setTitle("Configuration Portal");

  wm.setSaveParamsCallback(save_config_callback);
}

// Callback notifying us of the need to save configuration
static void save_config_callback() {
  debug_serial_println("save_config_callback invoked");
  m_save_config = true;
}

static void validate_and_save_params(
    spiffs_configuration_st &config,
    const WiFiManagerParameter &twelve_hour_format_param,
    const WiFiManagerParameter &date_display_frequency_param,
    const WiFiManagerParameter &slot_machine_cycle_frequency_param,
    const WiFiManagerParameter &local_temperature_display_frequency_param,
    const WiFiManagerParameter &open_weather_api_key_param) {
  if (!m_save_config) {
    return;
  }

  // Validate inputs
  debug_serial_println("Validating configuration parameters...");

  // OpenWeather API key must either be unset or the correct length
  if (open_weather_api_key_param.getValueLength() == 0 ||
      open_weather_api_key_param.getValueLength() == OPENWEATHER_API_KEY_LEN) {
    // Valid
    config.openweather_api_key = open_weather_api_key_param.getValue();
  } else {
    // Invalid API key. Use the previous value.
    debug_serial_printf("Invalid OpenWeather API Key [%s]\n",
                        open_weather_api_key_param.getValue());
  }

  set_eeprom_config_value_from_param(
      twelve_hour_format_param, EEPROM_12_HOUR_FORMAT_ADDRESS,
      EEPROM_12_HOUR_FORMAT_LOWER_BOUND, EEPROM_12_HOUR_FORMAT_UPPER_BOUND,
      nullptr, "Invalid Hour Format");

  set_eeprom_config_value_from_param(
      date_display_frequency_param, EEPROM_DATE_DISPLAY_FREQUENCY_ADDRESS,
      EEPROM_DATE_DISPLAY_FREQUENCY_LOWER_BOUND,
      EEPROM_DATE_DISPLAY_FREQUENCY_UPPER_BOUND, &g_task_display_date_handle,
      "Invalid Date Display Interval");

  set_eeprom_config_value_from_param(
      slot_machine_cycle_frequency_param,
      EEPROM_SLOT_MACHINE_CYCLE_FREQUENCY_ADDRESS,
      EEPROM_SLOT_MACHINE_CYCLE_FREQUENCY_LOWER_BOUND,
      EEPROM_SLOT_MACHINE_CYCLE_FREQUENCY_UPPER_BOUND, nullptr,
      "Invalid Slot Machine Cycle Interval");

  set_eeprom_config_value_from_param(
      local_temperature_display_frequency_param,
      EEPROM_LOCAL_TEMPERATURE_DISPLAY_FREQUENCY_ADDRESS,
      EEPROM_LOCAL_TEMPERATURE_DISPLAY_FREQUENCY_LOWER_BOUND,
      EEPROM_LOCAL_TEMPERATURE_DISPLAY_FREQUENCY_UPPER_BOUND, nullptr,
      "Invalid Local Temperature Display Interval");

  save_config_file(config);

  EEPROM.commit();

  m_save_config = false;
}

static void set_eeprom_config_value_from_param(
    const WiFiManagerParameter &param, uint8_t option_number,
    uint8_t lower_bound, uint8_t upper_bound, TaskHandle_t *task_handle,
    const char *error_msg_format_str) {
  uint8_t config_value = atoi(param.getValue());

  if (config_value < lower_bound || config_value > upper_bound) {
    // Invalid config value. Use previous value
    debug_serial_printf("%s [%d]. Using previous config value.\n",
                        error_msg_format_str, config_value);
    return;
  }

  // Resume or suspend the task depending on the config value
  if (task_handle) {
    config_value ? vTaskResume(*task_handle) : vTaskSuspend(*task_handle);
  }

  debug_serial_printf("\tStoring option: %d value: %d\n", option_number,
                      config_value);
  EEPROM.write(option_number, config_value);
}
