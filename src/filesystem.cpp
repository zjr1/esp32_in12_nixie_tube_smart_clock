#include "filesystem.h"

#include <ArduinoJson.h>
#include <FS.h>
#include <SPIFFS.h>

#include "arduino_debug.h"

#define JSON_CONFIG_FILE "/configuration.json"

// Save Config in JSON format
bool save_config_file(const spiffs_configuration_st& config) {
  debug_serial_println("Saving configuration...");

  // Create a JSON document
  StaticJsonDocument<512> json;

  if (config.openweather_api_key.isEmpty()) {
    json["openweather_api_key"] = nullptr;
  } else {
    json["openweather_api_key"] = config.openweather_api_key;
  }

  // Open config file
  File config_file = SPIFFS.open(JSON_CONFIG_FILE, "w");
  if (!config_file) {
    // Error, file did not open
    debug_serial_println("Failed to open config file for writing");
    return false;
  }

  // Print out JSON
  if (ARDUINO_DEBUG) {
    serializeJsonPretty(json, Serial);
    debug_serial_println("");
  }

  // Serialize JSON data to write to file
  if (serializeJson(json, config_file) == 0) {
    // Error writing file
    debug_serial_println("Failed to write to file");
    return false;
  }

  return true;
}

// Load existing configuration file and read configuration from SPIFFS JSON
bool load_config_file(spiffs_configuration_st* config) {
  // Uncomment if we need to format filesystem
  // SPIFFS.format();

  // May need to make it begin(true) first time you are using SPIFFS
  if (!SPIFFS.begin(false) && !SPIFFS.begin(true)) {
    debug_serial_println("Failed to mount file system");
    return false;
  }

  debug_serial_println("Mounted File System");

  if (!SPIFFS.exists(JSON_CONFIG_FILE)) {
    debug_serial_println("JSON config file does not exist");
    return false;
  }

  File configFile = SPIFFS.open(JSON_CONFIG_FILE, "r");
  if (!configFile) {
    debug_serial_println("Failed to open JSON config file");
    return false;
  }

  StaticJsonDocument<512> json;
  DeserializationError error = deserializeJson(json, configFile);
  if (error) {
    debug_serial_println("Failed to deserialize JSON config file");
    return false;
  }

  debug_serial_println("Deserialized JSON config file");

  if (ARDUINO_DEBUG) {
    serializeJsonPretty(json, Serial);
    debug_serial_println("");
  }

  if (json["openweather_api_key"].isNull()) {
    config->openweather_api_key = String{};
  } else {
    config->openweather_api_key = json["openweather_api_key"].as<String>();
  }

  return true;
}
