#include "ntp.h"

#include <Arduino.h>
#include <WiFi.h>

#include "arduino_debug.h"

const char* const c_ntp_server = "pool.ntp.org";
const long int c_gmt_offset_sec = -18000;
const int c_daylight_offset_sec = 3600;

void set_time_from_ntp() {
  if (!WiFi.isConnected()) {
    debug_serial_println(
        "Failed to set time from NTP since wifi is not connected.");
    return;
  }

  // Init and get the time
  bool ntp_time_configured = false;
  for (int i = 1; i <= 30; ++i) {
    configTime(c_gmt_offset_sec, c_daylight_offset_sec, c_ntp_server);
    vTaskDelay((250 * i) / portTICK_PERIOD_MS);  // Linear backoff

    if (print_local_time() == NTP_OK) {
      ntp_time_configured = true;
      break;
    }
  }

  if (!ntp_time_configured) {
    debug_serial_printf(
        "Failed to configure the time from the provided NTP server: %s\n",
        c_ntp_server);
  }
}

int print_local_time() {
  struct tm timeinfo;
  if (!getLocalTime(&timeinfo)) {
    Serial.println("Failed to obtain time");
    return NTP_ERROR;
  }

  Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");
  return NTP_OK;
}
